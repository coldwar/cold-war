var EventSource = require("../event/eventsrc");
var Game = require("../game/game");

module.exports = GameManager = EventSource.extend({
    init: function(server) {
        this._super();
        this.server = server;
        this.games = [];
        this.uidCounter = 0;
    },
    listGamesToPlayer: function(player) {
        var games = [];
        for (var i = 0; i < this.games.length; i++) {
            var g = this.games[i];
            if (g.started === false) {
                games.push({
                    id: g.id,
                    name: g.name,
                    playerCount: g.players.length,
                    maxPlayers: g.maxPlayers
                });
            }
        }
        player.client.emit("game_list", games);
    },
    listGamesToAllPlayers: function() {
        for (var i = 0; i < this.server.players.length; i++) {
            this.listGamesToPlayer(this.server.players[i]);
        }
    },
    sendGameToPlayer: function(game, player) {
        console.log("Sending game info to " + player.name);
        var players = [];
        for (var j = 0; j < game.players.length; j++) {
            var p = game.players[j];
            players.push({
                name: p.name,
                allegiance: p.allegiance
            });
        }
        player.client.emit("game_info", {
            id: game.id,
            players: players
        });
    },
    newGame: function(creator, name, maxPlayers) {
        this.removePlayerFromGame(creator);
        var game = new Game(this.uidCounter++, name, maxPlayers, this);
        game.addPlayer(creator);
        creator.setActiveGame(game);
        creator.setAllegiance(0);
        this.games.push(game);
        this.listGamesToAllPlayers();
        return game;
    },
    removePlayerFromGame: function(player) {
        if (player.activeGame !== false) {
            player.activeGame.removePlayer(player);
            if (player.activeGame.players.length === 0) this.removeGame(player.activeGame);
            player.activeGame = false;
            this.listGamesToAllPlayers();
        }
    },
    removeGame: function(game) {
        console.log("Removing game " + game.name);
        this.games.splice(this.games.indexOf(game), 1);
        this.listGamesToAllPlayers();
    }
});
