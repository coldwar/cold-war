var EventSource = require("../event/eventsrc");

module.exports = LobbyListener = EventSource.extend({
    init: function(server) {
        this._super();
        this.server = server;
    },
    registerPlayer: function(player) {
        var server = this.server;
        player.client.on("join_game", function(data) {
            for (var i = 0; i < server.gameManager.games.length; i++) {
                var g = server.gameManager.games[i];
                if (g.id === data.id && g !== player.activeGame && g.started === false) {
                    server.gameManager.removePlayerFromGame(player);
                    var slots = g.getOpenSlots();
                    var allegiance = slots[0] > slots[1] ? 0 : 1;
                    g.addPlayer(player);
                    player.setActiveGame(g);
                    player.setAllegiance(allegiance);

                    if (g.isFull()) {
                        g.start();
                    }

                    server.gameManager.listGamesToAllPlayers();
                }
            }
        });
        player.client.on("new_game", function(data) {
            var game = server.gameManager.newGame(player, data.name, data.maxPlayers);
            console.log("Created new game " + game.name);
        });
        player.client.on("get_game_info", function(data) {
            console.log("Got game info request");
            for (var i = 0; i < server.gameManager.games.length; i++) {
                var g = server.gameManager.games[i];
                if (g.id === data.id) {
                    server.gameManager.sendGameToPlayer(g, player);
                }
            }
        });
    }
});
