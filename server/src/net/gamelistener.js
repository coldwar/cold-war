var EventSource = require("../event/eventsrc");

module.exports = GameListener = EventSource.extend({
    init: function(server) {
        this._super();
        this.server = server;
    },
    registerPlayer: function(player) {
        var server = this.server;

        player.client.on("end_turn", function(data) {
            player.activeGame.endTurn(player, data);
        });

    }
});
