var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');

var port = process.env.PORT === undefined ? 88 : process.env.PORT;
var ip = process.env.IP === undefined ? "0.0.0.0" : process.env.IP;

app.listen(port, ip);

console.log("Listening at " + ip + ":" + port);

function handler (req, res) {
    var urlParts = req.url.split("/");
    console.log("Got request for " + req.url);
    if (urlParts.length > 2) {
        fs.readFile(req.url.substring(1, req.url.length), function (err, data) {
            if (err) {
                console.log("-> 404 Not found : " + err + " for file " + req.url.substring(1, req.url.length));
                res.writeHead(404);
                res.end();
            } else {
                var fParts = urlParts[urlParts.length - 1].split(".");
                var ext = fParts[fParts.length - 1];
                var contentType = "text/plain";
                if (ext === "js") contentType = "application/javascript";
                if (ext === "html") contentType = "text/html";
                if (ext === "css") contentType = "text/css";
                if (ext === "png") contentType = "image/png";
                if (ext === "gif") contentType = "image/gif";
                if (ext === "jpg") contentType = "image/jpeg";
                res.setHeader("Content-Type", contentType);
                res.writeHead(200);
                res.end(data);
            }
        });
    } else if (req.url === "/favicon.ico") {
        fs.readFile("client/assets/favicon.ico", function(err, data) {
            if (err) {
                console.log("--> Could not find favicon : " + err);
                res.writeHead(404);
                res.end();
            } else {
                res.writeHead(200);
                res.end(data);
            }
        });
    } else {
        var resp = "";
        resp += "<!DOCTYPE html>";
        resp += "<h1>Tepid Altercation Server</h1>";
        resp += "<hr>";
        resp += "<p>Currently running version 0.0.2</p>";
        resp += "<p>No data was requested</p>";
        res.writeHead(200);
        res.end(resp);
    }
}

var GameServer = require('./server');
var gameServer = new GameServer();

io.on('connection', function (socket) {
    console.log("Got connection");

    socket.on('login', function(data) {
        if (data.user.length > 0) gameServer.addClient(data.user, socket);
    });

    socket.on('disconnect', function () {
        gameServer.unloadClient(socket);
    });
});
