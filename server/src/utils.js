module.exports = {
    shuffle: function(a) {
        for (var i = a.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = a[j];
            a[j] = a[i];
            a[i] = temp;
        }
    }
};
