var EventSource = require("./event/eventsrc");
var Player = require("./game/player");

var GameListener = require("./net/gamelistener");
var LobbyListener = require("./net/lobbylistener");

var GameManager = require("./manager/gamemanager");

module.exports = Server = EventSource.extend({
    init: function() {
        this._super();

        this.players = [];
        this.uidCounter = 0;

        this.gameListener = new GameListener(this);
        this.lobbyListener = new LobbyListener(this);

        this.gameManager = new GameManager(this);

        console.log("Initialized Tepid Altercation game server");
    },
    addClient: function(name, client) {
        console.log("Adding client " + name);
        for (var i = 0; i < this.players.length; i++) {
            var p = this.players[i];
            if (p.name === name) {
                console.log("Login failed, same name");
                client.emit("login_fail", {reason: "Player with that name already connected"});
                return;
            }
        }

        client.emit("login_success");
        this.initPlayer(new Player(client, name, this.uidCounter++));
    },
    initPlayer: function(player) {
        console.log("Initializing player " + player.name);
        this.players.push(player);
        this.lobbyListener.registerPlayer(player);
        this.gameListener.registerPlayer(player);

        this.gameManager.listGamesToPlayer(player);

        this.updatePlayerLists();
    },
    updatePlayerLists: function() {
        var plist = [];
        for (var i = 0; i < this.players.length; i++) {
            plist.push(this.players[i].name);
        }
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].client.emit("player_list", plist);
        }
    },
    unloadClient: function(client) {
        console.log("Client disconnected");
        for (var i = this.players.length - 1; i >= 0; i--) {
            var p = this.players[i];
            if (p.client === client) {
                console.log("Unloading player " + p.name);
                this.gameManager.removePlayerFromGame(p);
                this.players.splice(i, 1);
            }
        }
        this.updatePlayerLists();
    }
});
