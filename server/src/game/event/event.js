var EventSource = require("../../event/eventsrc");

module.exports = Event = EventSource.extend({
    init: function(id, name, desc, confirm, deny, allegiance, onRun) {
        this._super();

        this.id = id;
        this.name = name;
        this.desc = desc;
        this.confirm = confirm;
        this.deny = deny;
        this.allegiance = allegiance;

        this.participants = [];
        this.onRun = onRun;

        this.new = true;
    },
    run: function() {
        console.log("Running event '" + this.name + "'");
        if (this.onRun !== undefined && this.onRun !== false) {
            this.onRun(this.participants);
        }
    },
    addParticipant: function(player) {
        this.participants.push(player);
    },
    isParticipating: function(id) {
        for (var i = 0; i < this.participants.length; i++) {
            var p = this.participants[i];
            if (p.id === id) return true;
        }
        return false;
    }
});
