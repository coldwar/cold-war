var EventSource = require("../../event/eventsrc");

module.exports = Event = EventSource.extend({
    init: function(game) {
        this._super();
        this.game = game;

        this.events = [];
    },
    addEvent: function(event) {
        this.events.push(event);
        for (var i = 0; i < this.game.players.length; i++) {
            var p = this.game.players[i];
            if ((event.allegiance === false || event.allegiance === p.allegiance) && !event.isParticipating(p.id)) {
                console.log("Adding event " + event.id + " to " + p.name);
                p.newEvents.push({
                    id: event.id,
                    name: event.name,
                    desc: event.desc,
                    confirm: event.confirm,
                    deny: event.deny
                });
            }
        }
    },
    getEvent: function(id) {
        for (var i = 0; i < this.events.length; i++) {
            var event = this.events[i];
            if (event.id === id) return event;
        }
        return false;
    },
    runEvents: function() {
        for (var i = this.events.length - 1; i >= 0; i--) {
            var e = this.events[i];
            if (!e.new) {
                e.run();
                this.events.splice(i, 1);
            } else {
                e.new = false;
            }
        }
    }
});
