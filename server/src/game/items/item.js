var EventSource = require("../../event/eventsrc");

module.exports = Item = EventSource.extend({
    init: function(id, name, desc, thumb, cost, unlocked, onUse, type) {
        this._super();

        this.id = id;
        this.name = name;
        this.desc = desc;
        this.thumb = thumb;
        this.cost = cost;
        this.type = type;
        this.unlocked = unlocked;
        this.onUse = onUse;

        this.count = 0;
    },
    unlock: function() {
        this.unlocked = true;
    },
    purchase: function() {
        this.count++;
        if (this.onUse !== undefined && this.onUse !== false) {
            this.onUse(this);
        }
    }
});
