var Item = require("./item");

module.exports = Weapon = Item.extend({
    init: function(id, name, desc, thumb, cost, unlocked, onUse, damage, chance, totalAnnihilateChance) {
        this._super(id, name, desc, thumb, cost, unlocked, onUse, 1);

        this.damage = damage;
        this.chance = chance;
        this.totalAnnihilateChance = totalAnnihilateChance;
    }
});
