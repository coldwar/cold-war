var Item = require("./item");

module.exports = MiscItem = Item.extend({
    init: function(id, name, desc, thumb, cost, unlocked, onUse) {
        this._super(id, name, desc, thumb, cost, unlocked, onUse, 2);
    }
});
