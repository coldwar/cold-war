var Item = require("./item");

module.exports = Defense = Item.extend({
    init: function(id, name, desc, thumb, cost, unlocked, onUse, percent, totalAnnihilatePercent, singleUse) {
        this._super(id, name, desc, thumb, cost, unlocked, onUse, 0);

        this.percent = percent;
        this.totalAnnihilatePercent = totalAnnihilatePercent;
        this.singleUse = singleUse;
    }
});
