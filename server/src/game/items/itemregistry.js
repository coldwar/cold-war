var EventSource = require("../../event/eventsrc");
var Weapon = require("./weapon");
var Defense = require("./defense");
var MiscItem = require("./miscitem");

module.exports = ItemRegistry = EventSource.extend({
    init: function(owner) {
        this._super();
        this.owner = owner;
        this.registry = [];
    },
    initRegistry: function() {
        this.registry = [];

        var self = this;

        this.addItem(new Weapon("atom_bomb", "Atom Bomb", "A basic atomic bomb. Gives a base 2.5% chance to hit.", "atom_bomb.jpg", 350, true, false, 25, 2.5, 2.5));
        this.addItem(new Weapon("h_bomb", "Hydrogen Bomb", "A new, more powerful bomb. This bomb has a 5% hit chance.", "h_bomb.jpg", 500, false, false, 50, 5, 7.5));
        this.addItem(new Weapon("icbm", "ICBM", "Long range, unmanned missile with a nuclear warhead. This weapon has a 15% hit chance.", "icbm.jpg", 1000, false, false, 100, 15, 17.5));
        this.addItem(new Weapon("mirv", "MIRV ICBM", "ICBMs with 3 warheads each, effectively tripling hit chance.", "mirv.jpg", 2500, false, false, 200, 30, 35));

        this.addItem(new Defense("sdi1", "ERINT Missile", "ERINT defense missiles which each decrease hit chance by 5%. Single use.", "sdi1.jpg", 350, false, false, 5, 0, true));
        this.addItem(new Defense("sdi2", "Directed Energy Weapon", "Directed Energy Weapons(DEW), each of which reduce hit chance by 10%. Single use.", "sdi1.jpg", 500, false, false, 10, 0, true));
        this.addItem(new Defense("sdi3", "Brilliant Pebbles", "Kinetic energy weapons you can shoot at enemy missiles from space. This reduces their hit chance by 30% each. Single use.", "sdi1.jpg", 1250, false, false, 30, 0, true));

        this.addItem(new Defense("a35", "A35 Anti Ballistic Missiles", "A35 Anti Ballistic Missiles which each decrease hit chance by 5%. Single use.", "abm.jpg", 350, false, false, 5, 0, true));
        this.addItem(new Defense("a35m", "A35m Anti Ballistic Missiles", "A35m Anti Ballistic Missiles, each of which reduce hit chance by 10%. Single use.", "abm.jpg", 500, false, false, 10, 0, true));
        this.addItem(new Defense("a135", "A135 Anti Ballistic Missiles", "A135 Anti Ballistic Missiles are the most advanced anti ballistic missiles. This reduces their hit chance by 30% each. Single use.", "abm.jpg", 1250, false, false, 30, 0, true));

        this.addItem(new MiscItem("spy", "Additional Spy", "Purchase an additional spy for your espionage division", "spy.jpg", 1500, false, function(item) {
            self.owner.spies++;
        }));

        console.log("Initialized registry for " + this.owner.name);
    },
    getItem: function(id) {
        for (var i = 0; i < this.registry.length; i++) {
            if (this.registry[i].id === id) return this.registry[i];
        }
        return false;
    },
    addItem: function(item) {
        this.registry.push(item);
    },
    getUnlocked: function() {
        var unlocked = [];
        for (var i = 0; i < this.registry.length; i++) {
            var item = this.registry[i];
            if (item.unlocked) unlocked.push(item);
        }
        return unlocked;
    },
    getCount: function(item) {
        var item = this.getItem(item);
        if (item !== false && item.unlocked) return item.count;
        return 0;
    },
    getUnlockedType: function(type) {
        var items = this.getUnlocked();
        var ofType = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].type === type) ofType.push(items[i]);
        }
        return ofType;
    },
    getUnlockedDefenses: function() {
        return this.getUnlockedType(0);
    },
    getUnlockedWeapons: function() {
        return this.getUnlockedType(1);
    },
    getUnlockedMisc: function() {
        return this.getUnlockedType(2);
    },
    getBestWeapon: function() {
        var ranks = ["atom_bomb", "h_bomb", "icbm", "mirv"];
        var weapons = this.getUnlockedWeapons();
        var selectedRank = 0;
        var selected = false;
        for (var i = 0; i < weapons.length; i++) {
            var w = weapons[i];
            for (var r = 0; r < ranks.length; r++) {
                if (ranks[r] === w.id && r > selectedRank) {
                    selectedRank = r;
                    selected = w;
                }
            }
        }
        return selected;
    }
});
