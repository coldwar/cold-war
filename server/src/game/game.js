var EventSource = require("../event/eventsrc");
var Utils = require("../utils");
var TreeFactory = require("./nodes/techtreefactory");
var EventManager = require("./event/eventmanager");

module.exports = Game = EventSource.extend({
    init: function(id, name, maxPlayers, gameManager) {
        this._super();
        this.id = id;
        this.name = name;
        this.maxPlayers = maxPlayers;
        this.gameManager = gameManager;

        this.players = [];

        this.started = false;

        this.turnCount = 0;

        this.treeFactory = new TechTreeFactory(this);

        this.eventManager = new EventManager(this);
    },
    addPlayer: function(player) {
        this.players.push(player);
    },
    removePlayer: function(player) {
        this.players.splice(this.players.indexOf(player), 1);
    },
    getOpenSlots: function() {
        var max = this.maxPlayers / 2;
        var counts = [max, max];

        for (var i = 0; i < this.players.length; i++) {
            var p = this.players[i];
            counts[p.allegiance]--;
        }

        return counts;
    },
    isFull: function() {
        return (this.players.length >= this.maxPlayers) || this.started;
    },
    start: function() {
        console.log("Starting game");
        this.started = true;
        for (var i = 0; i < this.players.length; i++) {
            var p = this.players[i];

            p.client.emit("game_start", {
                allegiance: p.allegiance
            });
            p.initResources();
            p.startTurn();
        }
    },
    getOtherPlayerData: function(player) {
        var otherPlayers = [];
        for (var j = 0; j < this.players.length; j++) {
            var other = this.players[j];
            if (other.id !== p.id) {
                otherPlayers.push({
                    id: other.id,
                    name: other.name,
                    allegiance: other.allegiance
                });
            }
        }
        return otherPlayers;
    },
    endTurn: function(player, data) {
        player.turnCompleted = true;
        player.turnData = data;
        player.processTurn();
        for (var i = 0; i < this.players.length; i++) {
            var p = this.players[i];
            if (p.turnCompleted === false) return;
        }

        //All players completed turn, begin next iteration
        Utils.shuffle(this.players);

        for (var i = 0; i < this.players.length; i++) {
            this.players[i].doTurnLogic();
        }

        this.eventManager.runEvents();

        //TODO On-turn logic here

        for (var i = this.players.length - 1; i >= 0; i--) {
            var p = this.players[i];
            if (p.population <= 0) this.endGameFor(p, "Lost all population!");
            if (p.happiness <= 0) this.endGameFor(p, "People revolt! Lost all happiness!");
        }

        if (this.players.length === 1) {
            var p = this.players[0];
            this.winGame(p, "You are the last man standing, so you win!");
        }

        for (var i = 0; i < this.players.length; i++) {
            this.players[i].startTurn();
        }

        this.turnCount++;
    },
    getPlayerById: function(id) {
        for (var i = 0; i < this.players.length; i++) {
            if (parseInt(this.players[i].id) === parseInt(id)) return this.players[i];
        }
        return false;
    },
    getOpposingPlayerIds: function(player) {
        var ids = [];
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].allegiance !== player.allegiance) ids.push(this.players[i].id);
        }
        return ids;
    },
    lockTech: function(id) {
        for (var i = 0; i < this.players.length; i++) {
            var p = this.players[i];
            var tech = p.tree.getChild(id);
            if (tech !== false) {
                tech.locked = true;
            }
        }
    },
    globalLog: function(msg) {
        console.log("[GLOBAL LOG] " + msg);
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].addLog("[GLOBAL] " + msg);
        }
    },
    endGameFor: function(player, victory) {
        this.globalLog("Game ended for " + player.name + ": " + victory);
        this.gameManager.removePlayerFromGame(player);
        player.client.emit("game_end", {victory: victory});
        player.inGame = false;
    },
    tieGame: function(msg) {
        for (var i = this.players.length - 1; i >= 0; i--) {
            this.endGameFor(this.players[i], msg === undefined ? "The game has tied between the remaining players c:" : msg);
        }
    },
    winGame: function(winner, winnerMsg, loserMsg) {
        for (var i = this.players.length - 1; i >= 0; i--) {
            var p = this.players[i];
            if (p.id !== winner.id) {
                this.endGameFor(p, loserMsg === undefined ? winner.name + " has won the game!" : loserMsg);
            }
        }
        this.endGameFor(winner, winnerMsg === undefined ? "Congratz, you've won the game!" : winnerMsg);
    }
});
