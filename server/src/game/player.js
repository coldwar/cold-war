var EventSource = require("../event/eventsrc");
var IntelManager = require("./intel/intelmanager");
var ItemRegistry = require("./items/itemregistry");

module.exports = Player = EventSource.extend({
    init: function(client, name, id) {
        this._super();
        this.client = client;
        this.name = name;
        this.id = id;

        this.activeGame = false;
        this.allegiance = 0;

        this.turnCompleted = false;

        this.itemRegistry = new ItemRegistry(this);
    },
    initResources: function() {
        this.science = this.allegiance ? 150 : 100;
        this.money = this.allegiance ? 1000 : 1500;
        this.population = this.allegiance ? 210 : 200;
        this.maxPopulation = 500;
        this.happiness = this.allegiance ? 100 : 105;
        this.spies = this.activeGame.maxPlayers / 2;
        this.spiesAvailable = this.spies;

        this.sciPopAlloc = Math.floor(this.population / 2);
        this.moneyPopAlloc = this.sciPopAlloc;

        this.defSpies = 0;
        this.spyAllocs = {}; //: Array(player_id:int=>spy_count:int)

        this.spyEffectiveness = 0.5;

        this.itemRegistry.initRegistry();

        this.tree = this.activeGame.treeFactory.createTree(this);

        this.intelManager = new IntelManager(this, this.activeGame.getOpposingPlayerIds(this));
        this.intelManager.populateBase();

        this.globalBuffs = []; //: Array(buff:Buff)
        this.buffs = {}; //: Array(player_id:int=>Array(buff:Buff))
        this.usedBuffs = []; //:Array(type:int=>Array(player_id:int=>used:boolean))

        for (var i = 0; i < 3; i++) {
            this.usedBuffs[i] = {};
        }

        this.moneyUpkeep = 0;
        this.moneyBoost = 0; //This is a percent!

        this.spyBoost = 0;

        this.popAdd = 0;

        this.taReduction = 0;
        this.defBonus = 0; //Applies to both defense and TA
        this.popDefIncrease = 0;

        this.newEvents = [];

        this.inGame = true;
        this.logMessages = [];
    },
    setActiveGame: function(game) {
        this.activeGame = game;
        this.client.emit("game_joined", {id: game.id});
    },
    setAllegiance: function(allegiance) {
        this.allegiance = allegiance;
        this.client.emit("allegiance", allegiance);
    },
    startTurn: function() {
        this.turnCompleted = false;

        this.addLog("It's your turn!");

        this.client.emit("start_turn", {
            science: Math.floor(this.science),
            money: Math.floor(this.money),
            population: this.population,
            happiness: this.happiness,
            spies: this.spiesAvailable,
            logMessages: this.logMessages,
            globalBuffs: this.globalBuffs,
            buffs: this.buffs,
            techTree: this.getTechData(),
            shops: this.getShopData(),
            turnCount: this.activeGame.turnCount,
            otherPlayers: this.activeGame.getOtherPlayerData(this),
            events: this.newEvents,
            upkeep: this.moneyUpkeep,
            defenses: this.getItemData(0),
            weapons: this.getItemData(1),
            spyAllocs: this.spyAllocs,
            sciPopAlloc: this.sciPopAlloc,
            moneyPopAlloc: this.moneyPopAlloc,
            popAvailable: this.population - this.sciPopAlloc - this.moneyPopAlloc
        });
        this.logMessages = [];
        this.newEvents = [];

        this.cleanBuffs();
    },
    getTechData: function(node) {
        var n = node === undefined ? this.tree : node;
        var tech = [];
        for (var i = 0; i < n.subNodes.length; i++) {
            var child = n.subNodes[i];
            if (child.isUnlockable() || child.unlocked) {
                var nData = {
                    id: child.id,
                    name: child.name,
                    desc: child.desc,
                    sciPrice: child.sciPrice,
                    moneyPrice: child.moneyPrice,
                    unlocked: child.unlocked,
                    thumb: child.thumb
                };
                if (child.subNodes.length > 0 && child.unlocked) nData.subNodes = this.getTechData(child);
                tech.push(nData);
            }
        }
        return tech;
    },
    getShopData: function() {
        var data = [];
        var unlocked = this.itemRegistry.getUnlocked();
        for (var i = 0; i < unlocked.length; i++) {
            var item = unlocked[i];
            data.push({
                item: item.id,
                name: item.name,
                desc: item.desc,
                thumb: item.thumb,
                cost: item.cost,
                count: item.count
            });
        }
        return data;
    },
    getItemData: function(type) {
        var items = this.itemRegistry.getUnlockedType(type);
        var data = [];
        for (var i = 0; i < items.length; i++) {
            var w = items[i];
            data.push({
                id: w.id,
                name: w.name,
                count: w.count
            });
        }
        return data;
    },
    /*
     * Does resource allocation. Called before doTurnLogic
     */
    processTurn: function() {
        var purchases = this.turnData.purchases;
        if (purchases !== undefined) {
            for (var i = 0; i < purchases.length; i++) {
                this.purchaseItem(purchases[i]);
            }
        } else {
            console.log("ERROR: Didn't recieve any purchase data from " + this.name);
        }
        console.log("Processed item purchases for player " + this.name);

        var tech = this.turnData.tech;
        if (tech !== undefined) {
            for (var i = 0; i < tech.length; i++) {
                this.purchaseTech(tech[i]);
            }
        } else {
            console.log("ERROR: Didn't recieve any tech data from " + this.name);
        }
        console.log("Processed tech purchases for player " + this.name);

        this.defSpies = this.turnData.defSpies;
        this.spiesAvailable = this.spies - this.defSpies;
        this.spyAllocs = this.turnData.spyAllocs;
        for (var p in this.spyAllocs) {
            this.spiesAvailable -= this.spyAllocs[p];
        }
        console.log("Allocated spies for player " + this.name + ", " + this.spiesAvailable + " more available");

        var popAvailable = this.population;
        this.sciPopAlloc = Math.min(popAvailable, this.turnData.sciPopAlloc);
        popAvailable -= this.sciPopAlloc;
        this.moneyPopAlloc = Math.min(popAvailable, this.turnData.moneyPopAlloc);

        console.log("Allocated population for player " + this.name + ", " + (this.population - this.sciPopAlloc - this.moneyPopAlloc) + " available");
    },
    /*
     * Does actual logic, like espionage, etc, called after resource allocation
     */
    doTurnLogic: function() {
        //Espionage, missles, etc
        var k = 0.2;
        var B = (this.maxPopulation / this.population) - 1;
        this.population = Math.floor(this.maxPopulation / (1 + B * Math.exp(-k)));
        this.population += this.popAdd;

        //I'm multiplying the pop allocs here by two so that an even split is the same
        //as the previous system... I don't know what the proper way you had decided to
        //do here is, since it wasn't here.
        var newMoney = Math.floor(((this.moneyPopAlloc * 2) * 3) * (this.happiness/200));
        this.money += newMoney + (newMoney * (this.moneyBoost / 100));
        this.money -= this.moneyUpkeep;

        this.science += Math.floor(((this.sciPopAlloc * 2) / 6) * (this.happiness/200));
        for (var p in this.spyAllocs) {
            var count = this.spyAllocs[p];
            console.log(count + " spies allocated from " + this.id + ":" + this.name + " to " + p);
            if (count > 0) {
                var player = this.activeGame.getPlayerById(p);
                if (player !== false) {
                    var result = player.doEspionage(count, this);
                    if (result !== false) {
                        this.addLog("--> Espionage attempt against " + player.name + " successful!");
                        result.unlock(this);
                        if (result.buff !== false) {
                            if (this.buffs[player.id] === undefined) this.buffs[player.id] = [];
                            this.buffs[player.id].push(result.buff);
                        }
                        this.addLog("[INTEL] " + result.name + ": " + result.desc);
                    } else {
                        this.addLog("--> Espionage attempt against " + player.name + " failed...");
                    }
                } else {
                    console.log("--> This player doesn't actually exist...");
                }
            } else {
                console.log("--> No spies, not calculating espionage.")
            }
        }

        var milAllocs = this.turnData.militaryAllocs;
        for (var i = 0; i < milAllocs.length; i++) {
            var m = milAllocs[i];
            var player = this.activeGame.getPlayerById(m.player);
            if (player !== false) {
                var buffs = this.getOffenseBuffs(player.id);
                for (var j = 0; j < m.allocs.length; j++) {
                    var alloc = m.allocs[j];
                    var weapon = this.itemRegistry.getItem(alloc.weapon);
                    var count = Math.min(alloc.count, weapon.count);
                    weapon.count -= count;
                    console.log(this.name + ":" + this.id + " is attacking " + player.name + ":" + player.id + " with " + count + " " + weapon.name);
                    if (count > 0) {
                        this.setUsedBuff(1, player.id, true);
                        player.addLog(this.name + " attacked you with " + count + " " + weapon.name);
                        this.addLog("You attacked " + player.name + " with " + count + " " + weapon.name);
                    }
                    for (var a = 0; a < count; a++) {
                        player.attack(this, weapon, buffs); //Attacking one by one here... I don't know if you want to amalgamate percentages.
                    }
                }
            }
        }

        var events = this.turnData.events;
        for (var i = 0; i < events.length; i++) {
            var e = this.activeGame.eventManager.getEvent(events[i]);
            if (e.allegiance === false || e.allegiance === this.allegiance) {
                console.log(this.name + ":" + this.id + " subscribed to event '" + e.name + "'");
                e.addParticipant(this);
            }
        }
    },
    cleanBuffType: function(type) {
        var used = this.usedBuffs[type];
        for (var player in used) {
            if (used[player] === true) {
                var buffs = this.buffs[player];
                if (buffs === undefined) return;
                for (var i = buffs.length - 1; i >= 0; i--) {
                    var b = buffs[i];
                    if (b.singleUse && b.type === type) buffs.splice(i, 1);
                }
            }
            used[player] = false;
        }
    },
    cleanBuffs: function() {
        this.cleanBuffType(0);
        this.cleanBuffType(1);
    },
    getBuffsOfType: function(playerID, type) {
        var buffs = this.buffs[playerID];
        if (buffs === undefined) return false;
        var ofType = [];
        for (var i = 0; i < buffs.length; i++) {
            var b = buffs[i];
            if (b.type === type) ofType.push(b);
        }
        return ofType;
    },
    getOffenseBuffs: function(playerID) {
        return this.getBuffsOfType(playerID, 1);
    },
    getDefenseBuffs: function(playerID) {
        return this.getBuffsOfType(playerID, 0);
    },
    /*
     * Puchase an item. Automatically deducts money and adds item to items.
     * Param1: i:string Item ID
     */
    purchaseItem: function(i) {
        var item = this.itemRegistry.getItem(i);
        if (item !== false && item.unlocked && this.money >= item.cost) {
            item.purchase();
            this.money -= item.cost;
            this.addLog("You bought an additional " + item.name);
        } else {
            this.addLog("You could not purchase item " + item.name);
        }
    },
    /*
     * Puchase a tech. Automatically deducts money, checks unlockable, and runs unlock function.
     * Param1: i:string Tech ID
     */
    purchaseTech: function(id) {
        if (this.tree.isChildUnlockable(id)) {
            var tech = this.tree.getChild(id);
            if (this.money >= tech.moneyPrice && this.science >= tech.sciPrice) {
                console.log(this.name + " has bought tech " + tech.id);
                tech.unlock();
                this.money -= tech.moneyPrice;
                this.science -= tech.sciPrice;
                this.addLog("You have now unlocked tech '" + tech.name + "', have fun with your new abilities.");
            } else {
                console.log(this.name + " didn't have enough money to buy tech " + tech.id);
                this.addLog("You couldn't afford to buy tech '" + tech.name + "', you cheater.");
            }
        }
    },
    doEspionage: function(count, other) {
        if (!this.inGame) return;

        console.log("--> Doing espionage against " + this.name + "...");
        var result = false;
        var offensePercent = other.spyEffectiveness + (other.spyBoost / 100);
        var defensePercent = this.spyEffectiveness;

        for (var i = 0; i < count; i++) {

            if (Math.random() < offensePercent) {
                for (var d = 0; d < this.defSpies; d++) {
                    if (Math.random() < defensePercent) {
                        return false;
                    }
                }
                console.log("--> --> Success");
                result = this.intelManager.getIntel(other);
            } else {
                console.log("--> --> Fail");
            }
        }

        return result;
    },
    addLog: function(msg) {
        this.logMessages.push(msg);
    },
    setUsedBuff: function(type, player, used) {
        this.usedBuffs[type][player] = used;
    },
    /*
     * INCOMING attack towards this player, from player specified
     */
    attack: function(player, weapon, buffs) {
        if (!this.inGame) return;
        var chance = weapon.chance - this.defBonus;

        if (buffs !== false) {
            console.log("--> Using " + buffs.length + " offensive buffs");
            for (var i = 0; i < buffs.length; i++) {
                chance += buffs[i].value * 100;
            }
        }

        var defBuffs = this.getDefenseBuffs(player.id);
        if (defBuffs !== false) {
            console.log("--> Using " + defBuffs.length + " defensive buffs");
            for (var i = 0; i < defBuffs.length; i++) {
                chance -= defBuffs[i].value * 100;
            }
            this.setUsedBuff(0, player.id, true);
        }

        var defenses = this.itemRegistry.getUnlockedDefenses();

        for (var i = 0; i < defenses.length; i++) {
            var d = defenses[i];
            for (var a = d.count; a > 0; a--) {
                chance -= d.percent;
                if (d.singleUse) d.count--;
                if (chance <= 0) return;
            }
        }

        console.log("--> Chance: " + chance);

        if (chance <= 0) return; //I figure if it can't hit, they can't be annihilated right?

        if (Math.random() < chance / 100) {
            var damage = weapon.damage * ((100 - this.popDefIncrease) / 100);
            this.population -= damage;
            this.addLog("You were hit by the attack!!! You lost " + damage + " population from a " + weapon.name);
            player.addLog("You hit them in the attack!!! They lost " + damage + " population from a " + weapon.name);
        }
    }
});
