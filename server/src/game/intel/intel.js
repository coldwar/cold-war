var EventSource = require("../../event/eventsrc");

module.exports = Intel = EventSource.extend({
    init: function(name, desc, singleUse, buff, onUnlock) {
        this._super();

        this.name = name;
        this.desc = desc;
        this.singleUse = singleUse;

        this.buff = buff;

        this.onUnlock = onUnlock;
    },
    unlock: function(unlocker) {
        if (this.onUnlock !== undefined) this.onUnlock(this, unlocker);
    }
});
