var EventSource = require("../../event/eventsrc");
var Intel = require("./intel");

module.exports = IntelManager = EventSource.extend({
    /*
     * Param1: player:Player Owner player instance
     * Param2: others:Array(player_id:int) Opposing player ids
     */
    init: function(player, others) {
        this._super();

        this.player = player;
        this.available = {}; //Array(player_id:string=>Array(intel:Intel))
        for (var i = 0; i < others.length; i++) {
            this.available[others[i]] = [];
        }
    },
    populateBase: function() {
        var player = this.player;
        //UNLOCKED TECHNOLOGIES...
        this.addAvailable(new Intel("Hydrogen Bomb", "WBR", true, false, function(intel, unlocker) {
            intel.desc = player.name + " has" + (player.tree.isChildUnlocked("h_bomb") ? " " : " not ") + "unlocked the Hydrogen Bomb";
        }));
        this.addAvailable(new Intel("ICBM", "WBR", true, false, function(intel, unlocker) {
            intel.desc = player.name + " has" + (player.tree.isChildUnlocked("icbm") ? " " : " not ") + "unlocked the ICBM";
        }));
        this.addAvailable(new Intel("MIRV", "WBR", true, false, function(intel, unlocker) {
            intel.desc = player.name + " has" + (player.tree.isChildUnlocked("mirv") ? " " : " not ") + "unlocked the MIRV";
        }));

        if (player.allegiance === 0) {
            this.addAvailable(new Intel("SDI I", "WBR", true, false, function(intel, unlocker) {
                var u = player.tree.isChildUnlocked("sdi1");
                intel.desc = player.name + " has" + (u ? " " : " not ") + "unlocked SDI I.";
                if (u) intel.desc += " This allows them to purchase ERINT missiles.";
            }));
            this.addAvailable(new Intel("SDI II", "WBR", true, false, function(intel, unlocker) {
                var u = player.tree.isChildUnlocked("sdi2");
                intel.desc = player.name + " has" + (u ? " " : " not ") + "unlocked SDI II.";
                if (u) intel.desc += " It's more effective than SDI I. They can defend themselves with Directed Energy Weapons. Don’t attack with less than hydrogen bombs.";
            }));
            this.addAvailable(new Intel("SDI III", "WBR", true, false, function(intel, unlocker) {
                var u = player.tree.isChildUnlocked("sdi3");
                intel.desc = player.name + " has" + (u ? " " : " not ") + "unlocked SDI III.";
                if (u) intel.desc += " They can fire \"Brilliant Pebbles\" at your weapons. Attacking will be more difficult.";
            }));
        } else {
            this.addAvailable(new Intel("a35 Ballistic Missiles", "WBR", true, false, function(intel, unlocker) {
                intel.desc = player.name + " has" + (player.tree.isChildUnlocked("a35") ? " " : " not ") + "unlocked a35 ballistic missiles";
            }));
            this.addAvailable(new Intel("a35m Ballistic Missiles", "WBR", true, false, function(intel, unlocker) {
                var u = player.tree.isChildUnlocked("a35m");
                intel.desc = player.name + " has" + (u ? " " : " not ") + "unlocked a35m ballistic missiles.";
                if (u) intel.desc += " Attacking will be more difficult.";
            }));
            this.addAvailable(new Intel("a135 Ballistic Missiles", "WBR", true, false, function(intel, unlocker) {
                intel.desc = player.name + " has" + (player.tree.isChildUnlocked("a135") ? " " : " not ") + "unlocked a135 ballistic missiles";
            }));
        }

        //OFFENSIVE CAPABILITIES...
        this.addAvailable(new Intel("Atom Bombs", "WBR", false, false, function(intel, unlocker) {
            intel.desc = player.name + " has " + player.itemRegistry.getCount("atom_bomb") + " atom bombs";
        }));
        this.addAvailable(new Intel("Atom Bombs Estimate", "WBR", false, false, function(intel, unlocker) {
            var count = player.itemRegistry.getCount("atom_bomb") - Math.floor(Math.random() * 10);
            intel.desc = player.name + " has more than " + count + " atom bombs. We don't know how many...";
        }));
        this.addAvailable(new Intel("Atom Bombs Estimate", "WBR", false, false, function(intel, unlocker) {
            var count = player.itemRegistry.getCount("atom_bomb") + Math.floor(Math.random() * 10);
            intel.desc = player.name + " has less than " + count + " atom bombs. We don't know how many...";
        }));
        this.addAvailable(new Intel("Offensive Strategies",
        "Enclosed are some of the offensive strategies used by " + player.name + ". I hope you can use them well." +
        "(Provides a one time 5% defence boost against player, used automatically in the event of attack).", false, {
            singleUse: true,
            type: 0,
            value: 0.05
        }));

        //DEFENSIVE CAPABILITIES...
        this.addAvailable(new Intel("Defensive Strategies",
        "I’ve found some of the defensive missile locations around " + player.name + "’s cities. Hopefully this will help you circumvent their defenses. Good luck. " +
        "(Provides a 5% bonus to attacking against the specified nation).", false, {
            singleUse: true,
            type: 1,
            value: 0.05
        }));

        //MISC KNOWLEDGE...
        this.addAvailable(new Intel("Relative Happiness", "WBR", false, false, function(intel, unlocker) {
            intel.desc = player.name + "'s happiness is " + (unlocker.happiness > player.happiness ? "less than" : "greater than") + " yours";
        }));
        this.addAvailable(new Intel("Happiness Level", "WBR", false, false, function(intel, unlocker) {
            var happy = player.happiness;
            intel.desc = player.name + "'s population is close to rioting. They have less than 30 happiness right now.";
            if (happy > 30) {
                intel.desc = "The population of " + player.name + " is suffering. Their happiness is less than 100";
            }
            if (happy > 100) {
                intel.desc = player.name + " is satisfying their population, causing them to have a happiness level greater than 100";
            }
        }));
        this.addAvailable(new Intel("Economy Relative", "WBR", false, false, function(intel, unlocker) {
            if (unlocker.money > player.money) {
                intel.desc = "Player’s debts are stacking up. They don’t have a lot of money left. I can’t say for sure, but it’s less than yours.";
            } else {
                intel.desc = "Player is going through a production boom. Their current financial situation is better than yours.";
            }
        }));
        this.addAvailable(new Intel("Economy Info", "WBR", false, false, function(intel, unlocker) {
            var money = player.money;
            intel.desc = player.name + " can’t buy anything right now. There is less than $500 billion in their bank.";
            if (money > 500) {
                intel.desc = "The finances of " + player.name + " are currently very prohibitive. They have less than $5000 billion in their bank.";
            }
            if (money > 5000) {
                intel.desc = player.name + " has been saving for some time now: they have over $5000 billion in their reserve.";
            }
        }));
        this.addAvailable(new Intel("Population Relative", "WBR", false, false, function(intel, unlocker) {
            if (unlocker.population > player.population) {
                intel.desc = player.name + " doesn’t have a lot of population at the moment. Must be all the fighting. They have less than you.";
            } else {
                intel.desc = "Must be a rubber shortage in " + player.name + "'s country, as their population has increased a lot. They have more population than you.";
            }
        }));
        this.addAvailable(new Intel("Population Info", "WBR", false, false, function(intel, unlocker) {
            var pop = player.population;
            intel.desc = player.name + " is almost eradicated. They have less than 10 million population.";
            if (pop > 10) {
                intel.desc = "There isn’t very much population in " + player.name + "’s country right now. I’d say less than 100 million.";
            }
            if (pop > 100) {
                intel.desc = "There is a modest population in " + player.name + "’s country right now. 100 million at least.";
            }
            if (pop > 300) {
                intel.desc = "The population of " + player.name + "’s country is greater than 300 million.";
            }
        }));
        this.addAvailable(new Intel("Science Relative", "WBR", false, false, function(intel, unlocker) {
            if (unlocker.science > player.science) {
                intel.desc = player.name + " must be close to a breakthrough. They have a lot of stored science. More than us.";
            } else {
                intel.desc = "Science output in " + player.name + "’s country is really low right now: they don’t have as much as us.";
            }
        }));
        this.addAvailable(new Intel("Science Info", "WBR", false, false, function(intel, unlocker) {
            var science = player.science;
            intel.desc = player.name + " isn’t doing well technologically. Their science is below 50.";
            if (science > 50) {
                intel.desc = "Stored science in " + player.name + "’s country is low right now: less than 200.";
            }
            if (science > 200) {
                intel.desc = "There is a modest amount of science in " + player.name + "’s country. 200 at least.";
            }
            if (science > 400) {
                intel.desc = player.name + " has a lot of science stored up. Hopefully that means they aren’t producing much right now. In any case, they have more than 400.";
            }
        }));
    },
    addAvailable: function(intel) {
        for (p in this.available) {
            this.available[p].push(intel);
        }
    },
    /*
     * Randomly draw an intel out of the player's pool
     * Param1: player:Player Opposing player instance
     * Returns: intel:Intel
     */
    getIntel: function(player) {
        var available = this.available[player.id];
        var i = Math.floor(Math.random() * available.length);
        var intel = available[i];
        if (intel.singleUse) {
            available.splice(i, 1);
        }
        return intel;
    }
});
