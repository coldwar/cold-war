var EventSource = require("../../event/eventsrc");
var Node = require("./node");
var Intel = require("../intel/intel");
var Event = require("../event/event");
var Utils = require("../../utils");

module.exports = TechTreeFactory = EventSource.extend({
    init: function(game) {
        this.game = game;
    },
    createTree: function(player) {
        var game = this.game;

        var tree = new Node(player, "tree", player.name + "'s Tree", "Your tech tree", true);

        var militaryBranch = new Node(player, "military", "military.png", "Military", "Military strategies and tech to crush your enemies", true);
        var offenseBranch = new Node(player, "offense", "offense.png", "Offense", "Blow 'em to smithereens", true);
        var aBomb = new Node(player, "atom_bomb", "abomb.gif", "Atomic Bomb", "A basic atomic bomb. Gives a base 5% chance to hit", true);
        var hBomb = new Node(player, "h_bomb", "hbomb.jpg", "Hydrogen Bomb", "A new, more powerful bomb. This bomb has a 10% base chance to hit.", false, 150, 800, ["h_bomb"], this.createItemIntel(player, "h_bomb"));
        var icbm = new Node(player, "icbm", "icbm.jpg", "ICBM", "Long range, unmanned missile with a nuclear warhead. This weapon has a 30% hit chance.", false, 375, 1200, ["icbm"], this.createItemIntel(player, "icbm"));
        var mirv = new Node(player, "mirv", "mirv.jpg", "MIRV", "ICBMs with 3 warheads each, effectively tripling hit chance.", false, 500, 1500, ["mirv"], this.createItemIntel(player, "mirv"));
        var defenseBranch = new Node(player, "defense", "def.png", "Defense", "Defend yo'self man.", true);

        var espionageBranch = new Node(player, "espionage", "esp.png", "Espionage", "I can seeee youuuu", true);
        var satImagery = new Node(player, "sat_image", "satellite.jpg", "Satellite Imagery", "Give more accurate locations, allowing your spies to become 10% more effective at stealing information.", false, 200, 100, false, false, function() {
            player.spyBoost += 10;
        });
        var expandAgency = new Node(player, "exp_agency", "expand.png", "Expand Agency", "Allows for the training of new spies.", false, 0, 600, ["spy"], false);
        var advancedCommunication = new Node(player, "adv_com", "advancedcom.png", "Advanced Communications", "Allows your spies to use more secure methods of communication, thus giving them a 10% higher chance to provide you with information.", false, 400, 1000, false, false);
        var diplomacyBranch = new Node(player, "diplomacy", "diplomacy.png", "Diplomacy", "Use your smooth talkin' baby", true);
        var propaganda = new Node(player, "propaganda", "propaganda.png", "Propaganda", "You are so great. Like, really great. Make sure everyone knows how great you are.", true);

        var summitSeries = new Node(player, "summit", "summit.png", "Vie for Summit Series", "Try to get your country involved in the Summit Series. The winners get a bonus to happiness, but all the losers get reductions", false, 0, 0, false, false, function() {
            var event = new Event("summit", "Summit Series", player.name + " has decided to host the Summit Series, but needs players. Enter your nation into the competition. The winner will get happiness benefits, but the losers lose happiness. Fitting.", "Well, you know, like, man, lets, you know, kick their, like, asses man.", "Uh... No. They smell like sardines.", false, function(participants) {
                Utils.shuffle(participants);
                var hMod = -10;
                for (var i = 0; i < participants.length; i++) {
                    var p = participants[i];
                    if (i < 2) {
                        p.happiness += 20;
                        p.addLog("Congrats, you came in number " + (i + 1) + " in the summit series; you gain 20 happiness");
                    } else {
                        p.happiness += hMod;
                        p.addLog("You lost the summit series pretty bad, coming in number " + (i + 1) + "; you lost " + (-hMod) + " happiness");
                        hMod -= 10;
                    }
                }
            });
            game.eventManager.addEvent(event);
            game.lockTech("summit");
        });
        var spaceRace = new Node(player, "space", "space.png", "Space Race", "The race to space", true);

        var mannedFlight = new Node(player, "manned_flight", "mannedflight.jpg", "Launch manned flight", "Send an astronaut into orbit. This increases happiness", false, 400, 0, false, false, function() {
            player.happiness += 10;
        });
        var moonMission = new Node(player, "moon", "moon.jpg", "Launch moon mission", "Land astronauts on the moon. Happiness Benefit", false, 800, 0, false, false, function() {
            player.happiness += 20;
        });

        var posters = new Node(player, "posters", "poster.jpg", "Posters", "WILL BE SET", false, 0, 500, false, false, function() {
            player.happiness += 5;
        });
        var radioPresence = new Node(player, "radio", "radio.png", "Radio presence", "WILL BE SET", false, 0, 1000, false, false, function() {
            player.happiness += 10;
        });
        var tvPresence = new Node(player, "tv", "tv.jpg", "TV presence", "WILL BE SET", false, 0, 2000, false, false, function() {
            player.happiness += 20;
        });

        var proposeSTART = new Node(player, "start", "start.jpg", "Propose START", "After relatively unsuccessful Arms Limitation treaties such as SALT, propose START, the Strategic Arms Reduction Treaty. If ratified by every nation, the war ends peacefully. If not, all consigned get a huge science and happiness boost.", false, 0, 0, false, false, function() {
            var event = new Event("start", "START Treaty", player.name + " is proposing START, the Strategic Arms Reduction Treaty. If you agree, you will get a happiness and science boost, and lose 5 bombs. If everyone agrees, the game ends peacefully, and points are calculated to find a winner.", "Lets end this thing!", "What the hell? Peace is for the weak!", false, function(participants) {
                if (participants.length >= game.players.length) {
                    game.tieGame("The START treaty was a great success. You and every remaining player have won the game. Good job getting out alive!");
                } else {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        p.addLog("Some seem to prefer war over peace. The START treaty failed. :c In any case, you get your boost to science and happiness");
                        p.science += 250;
                        p.happiness += 30;
                        var weapons = p.itemRegistry.getUnlockedWeapons();
                        Utils.shuffle(weapons);
                        for (var i = 0; i < weapons.length; i++) {
                            var w = weapons[i];
                            if (w.count > 0) {
                                w.count--;
                                break;
                            }
                        }
                    }
                }
            });

            game.eventManager.addEvent(event);
            game.lockTech("start");
        });
        var attemptBlackmail = new Node(player, "blackmail", "blackmail.png", "Attempt Blackmail", "Assemble your secrets about the other nations and attempt political blackmail. You give the other nations - including the other capitalists - the option to trust you, destroy most of their weapons, and hope you do the same. If they they don't, you fire everything, against every country, in hopes of complete victory. If you fail, you will likely die.", false, 0, 0, false, false, function() {
            var event = new Event("blackmail", "Blackmail", "Player is trying to blackmail you! What nerve! They say they will release nasty information about you, which will cause your populations happiness to halve. You can ignore them and fire nukes at them, or if you agree to capitulate, you have to give up your weapons. If everyone does this, the game ends and Player wins a solo victory, however some nations may not go for this. If even one nation doesn't agree, " + player.name + " will fire wildly at everybody. There is a chance for total destruction here, or he might get destroyed by you in the process. You will still lose all of your offensive weapons if you give in and others don’t. No cost.", "I give in! Please don't hurt me!", "Screw that. He can't hurt me!", false, function(participants) {
                if (participants.length < game.players.length) {
                    if (Math.random() < 0.3) {
                        game.tieGame("After " + player.name + " fired nukes everywhere, all life on Earth was eliminated. You all lose.");
                    } else {
                        game.endGameFor(player, "You tried to fire nukes everywhere, but the other nations were faster.");
                    }
                } else {
                    game.winGame(player, "All other players submitted to your blackmail! You win!");
                }
            });

            event.addParticipant(player);

            game.eventManager.addEvent(event);
            game.lockTech("blackmail");
        });
        var apolloSoyuz = new Node(player, "apollo-soyuz", "apollosoyuz.jpg", "Propose Apollo-Soyuz mission", "WILL BE SET", false, 0, 0, false, false, function() {
            var event = new Event("apollo-soyuz", "Apollo-Soyuz", player.name + " has proposed the joint Apollo Soyuz missions. If every player participates, the game ends and points are counted, however if only some of the players participate, every one of them gains a free technology.", "Hehe you said joint. Sign me up", "Don’t be so immature. Not a chance.", false, function(participants) {
                if (participants.length >= game.players.length) {
                    game.tieGame("The joint Apollo-Soyuz mission was a great success. You and every remaining player have won the game. Good job getting out alive!");
                } else {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        p.addLog("Welp, seems someone's a stick in the mud. Only some players accepted the deal. In any case, since you valued cooperation, you get a free tech for your efforts");
                        var techs = p.tree.getAllUnlockable();
                        var tech = techs[Math.floor(Math.random() * techs.length)];
                        tech.unlock();
                        p.addLog("You unlocked '" + tech.name + "'");
                    }
                }
            });
            game.eventManager.addEvent(event);
            game.lockTech("apollo-soyuz");
        });

        tree.addChild(militaryBranch);
            militaryBranch.addChild(offenseBranch);
                offenseBranch.addChild(aBomb);
                    aBomb.addChild(hBomb);
                        hBomb.addChild(icbm);
                            icbm.addChild(mirv);
            militaryBranch.addChild(defenseBranch);

        tree.addChild(espionageBranch);

        tree.addChild(diplomacyBranch);
            diplomacyBranch.addChild(propaganda);
                propaganda.addChild(summitSeries);
            diplomacyBranch.addChild(spaceRace);


        if (player.allegiance === 0) { //For future reference, we store this as an integer 0=capitalist/1=communist.
            var invadeBayOfPigs = new Node(player, "bay_pigs", "bay.jpg", "Invade Bay Of Pigs", "Invade the Bay of Pigs. Sparks the Cuban Missile Crisis. If you invade successfully, you immediately gain 1 soviet bomb from every communist player.", false, 0, 1000, false, false, function() {
                var event = new Event("bay_pigs", "Invade Cuba", player.name + " has invaded the bay of pigs. Would you like to help them? If you do, it will cost money, but you will also get nukes if you win. The more countries involved, the greater the chance of success.", "I'm running low on bacon, I'll do it.", "Nah, man, just chillax bro.", 0, function(participants) {
                    var normalizedPercent = participants.length / (game.maxPlayers / 2);
                    var success = Math.random() > normalizedPercent / 2;
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id === player.id || p.money > 1000) {
                            if (p.id !== player.id) p.money -= 1000;
                            p.population -= 20;
                            p.happiness -= 10;
                            if (success) {
                                p.addLog("Huzzah! You successfully invaded Cuba! You got 5 of your best nukes for your effort!");
                                p.itemRegistry.getBestWeapon().count += 5;
                            } else {
                                p.addLog("Damnit. You fought the law and the law won. Invasion of Cuba failed.");
                            }
                        } else {
                            p.addLog("You didn't end up having enough money to commit to the invasion of Cuba." + (success ? " Sucks to be you. They ended up winning." : " This works out well for you, though. They lost... Badly..."));
                        }
                    }
                    if (success) {
                        game.globalLog("The capitalists successfully invaded Cuba!");
                    } else {
                        game.globalLog("The capitalists were unable to invade Cuba.");
                    }
                });

                game.eventManager.addEvent(event);
                game.lockTech("bay_pigs");
            });
            var vietnamWar = new Node(player, "vietnam", "vietnam.png", "Commit to Vietnam", "Commit footsoldiers to the Vietnam War. If you win, you get the cost back and some extra, in addition to other benefits.", false, 0, 2000, false, false, function() {
                var event = new Event("vietnam", "Commit to Vietnam", player.name + " has committed to the vietnam war... Would you like to help them? Like usual, the benefits and consqeunces apply to you as well if you help.", "GOOD MORNING, VIETNAM!!", "Nope", 0, function(participants) {
                    var normalizedPercent = participants.length / (game.maxPlayers / 2);
                    var success = Math.random() > normalizedPercent / 2;
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id === player.id || p.money > 2000) {
                            if (p.id !== player.id) p.money -= 2000;
                            p.population -= 30;
                            p.happiness -= 10;
                            if (success) {
                                p.addLog("Huzzah! You successfully beat Vietnam!");
                                p.money += 3500;
                                p.happiness += 15;
                            } else {
                                p.addLog("Damnit. You lost against the Vietnamese insurgents. You pay war reparations worth $750 billion");
                                p.money -= 750;
                            }
                        } else {
                            p.addLog("You didn't end up having enough money to commit to Vietnam." + (success ? " Sucks to be you. They ended up winning." : " This works out well for you, though. They lost... Badly..."));
                        }
                    }
                    if (success) {
                        game.globalLog("The capitalists successfully invaded Vietnam!");
                    } else {
                        game.globalLog("The capitalists were unable to invade Vietnam.");
                    }
                });

                event.addParticipant(player);

                game.eventManager.addEvent(event);
                game.lockTech("vietnam");
            });
            var createNato = new Node(player, "nato", "nato.png", "Create Nato", "Propose NATO to the other Capitalist Nations. Any nation who accepts will benefit from the alliance, by getting a permanent 5% boost to their defense, which will reduce enemy hit chance.", false, 0, 1000, false, false, function() {
                var event = new Event("nato", "Join NATO", player.name + " has proposed NATO. This provides a defense boost. It costs 1000 money", "Sign me up", "Not a chance, bucko", 0, function(participants) {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id === player.id || p.money > 1000) {
                            if (p.id !== player.id) p.money -= 1000;
                            p.defBonus += 5;
                            p.addLog("You have joined NATO!");
                        } else {
                            p.addLog("You didn't have enough money to join NATO! Oh well. You're on your own kid.");
                        }
                    }
                    game.globalLog("NATO has formed!");
                });

                event.addParticipant(player);

                game.eventManager.addEvent(event);
                game.lockTech("nato");
            });

            var sdiI = new Node(player, "sdi1", "sdi.jpg", "SDI I", "The first stage of SDI. You can purchase ERINT defense missiles which each decrease hit chance by 5%", false, 100, 500, ["sdi1"], this.createItemIntel(player, "sdi1"));
            var sdiII = new Node(player, "sdi2", "sdi.jpg", "SDI II", "The second stage of SDI. You can purchase Directed Energy Weapons(DEW), each of which reduce hit chance by 10%", false, 200, 850, ["sdi2"], this.createItemIntel(player, "sdi2"));
            var sdiIII = new Node(player, "sdi3", "sdi.jpg", "SDI  III", "You can pioneer Space Based Programs. This Tech gives an initial 10% hit decrease due to improved sensor technology, as well as opening Brilliant Pebbles for purchase, which decrease hit chance by 30% each.", false, 450, 1200, ["sdi3"], this.createItemIntel(player, "sdi3"));

            var spies = new Node(player, "spies", "spies.jpg", "Spies", "Your offensive spy strategies. Offending college students worldwide since 2013.", true);
            var cia = new Node(player, "cia", "cia.png", "CIA", "The CIA leads espionage operations in communist countries. Initially unlocks spies equivalent to the number of enemy players. Allows the placement of these spy’s with the following statistics: 50% to steal information in a given turn, or 50% chance to prevent information from being stolen if placed in friendly territory.", true);
            var counterEspionage = new Node(player, "counter_esp", "counter-esp.png", "Counter Espionage", "Screw 'em", true);
            var fbi = new Node(player, "fbi", "fbi.jpg", "FBI", "The FBI is in charge of Counter Espionage. They have a 1% chance to discover an enemy spy every turn.", true);
            var mcCarthyism = new Node(player, "mcarthy", "mccarthy.jpg", "McCarthyism", "Establishing McCarthyism gives an increased chance(+20%) to discover enemy spies every turn, but lowers happiness.", true, 0, 1000, false, false, function() {
                player.happiness -= 20;
                player.spyBoost += 20;
            });
            var establishFCDA = new Node(player, "fcda", "fcda.png", "FCDA", "The Federal Civil Defense Administration increases defensive capabilities, reducing the population loss if attacked by 20%. Does not affect hit chance", false, 0, 1050, false, false, function() {
                player.popDefIncrease = 20;
            });
            var establishOCDM = new Node(player, "ocdm", "ocdm.jpg", "OCDM", "The OCDM embodied a coordinated effort to increase wartime stability. Gives 40% reduced population loss on attack when established. Does not stack with FCDA.", false, 0, 2500, false, false, function() {
                player.popDefIncrease = 40;
            });

            var supportWestBerlin = new Node(player, "berlin", "berlin.jpg", "Support West Berlin", "Send aid to Berlin in the form of food dropped from planes. Costs money, but affects both communist happiness and your own. The more nations involved, the less money it costs.", false, 0, 1000, false, false, function() {
                var event = new Event("berlin", "Support West Berlin", "Do you want to help " + player.name + " support Berlin? It costs money per turn, but the cost goes down the more nations that are involved. Increases happiness by 15.", "Send the planes", "Screw that", 0, function(participants) {
                    var upkeep = 100 / (participants.length / (game.maxPlayers / 2));
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        p.addLog("You are now supporting West Berlin");
                        p.moneyUpkeep += upkeep;
                        p.happiness += 15;
                    }
                    game.globalLog("The capitalists are supporting West Berlin!");
                });

                event.addParticipant(player);

                game.eventManager.addEvent(event);
                game.lockTech("berlin");
            });
            var launchExplorer1 = new Node(player, "explorer", "explorer.jpg", "Launch Explorer 1", "Launch the satellite Explorer I, thus commencing your journey into space. Provides happiness benefit", false, 0, 0, false, false, function() {
                player.happiness += 10;
            });
            var buildISS = new Node(player, "iss", "iss.jpg", "Build ISS", "Participate in building the ISS.", false, 600, 600, false, false, function() {
                var event = new Event("iss", "Construct ISS", player.name + " has proposed ISS. Would you like to add your funds and science to the project? Your science will increase more every turn (5%) due to collaboration with other nations, but it will have a one time cost of 500 science and 500 money.", "I would love to", "Nope", 0, function(participants) {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id !== player.id) {
                            if (p.science > 500 && p.money > 500) {
                                p.moneyBoost += 5;
                                p.science -= 500;
                                p.money -= 500;
                            } else {
                                p.addLog("You did not have enough money and science to participate in the building of ISS");
                            }
                        }
                    }
                    player.moneyBoost += 5;
                    game.globalLog("The capitalists built the ISS!");
                });

                event.addParticipant(player); //They can't weasle out of this one.

                game.eventManager.addEvent(event);
                game.lockTech("iss");
            });

            //--- Special Prereqs ---

            proposeSTART.prereqs = [sdiIII, establishFCDA, advancedCommunication];
            attemptBlackmail.prereqs = [establishOCDM, tvPresence, moonMission];
            apolloSoyuz.prereqs = [radioPresence, sdiIII, icbm];

            //--- Special Descs ---
            posters.desc = "You commission anti-communist posters and hang them around the city, increasing happiness.";
            radioPresence.desc = "Radios will now play anti-communist ads and discuss government-suggested topics. This further increases happiness, but also increases the negative effects of communist victory.";
            tvPresence.desc = "You start showing \"duck and cover\" films, as well as other television propaganda. This magnifies the effect of radio ads";
            apolloSoyuz.desc = "Allow the communists access to your space technology in return for your own. If all other countries agree to your proposal, the cold war ends. Otherwise, participating nations each gain a free technology as a result of new information.";

            aBomb.addChild(createNato);
            icbm.addChild(vietnamWar);
            icbm.addChild(invadeBayOfPigs);
            mirv.addChild(proposeSTART);

            defenseBranch.addChild(sdiI);
                sdiI.addChild(sdiII)
                    sdiII.addChild(sdiIII)

            espionageBranch.addChild(spies);
                spies.addChild(cia);
                    cia.addChild(satImagery);
                        satImagery.addChild(expandAgency);
                            expandAgency.addChild(advancedCommunication);
                                advancedCommunication.addChild(attemptBlackmail); //also requires OCDM TV MOON MISSION

            espionageBranch.addChild(counterEspionage);
                counterEspionage.addChild(fbi);
                    fbi.addChild(mcCarthyism);
                        mcCarthyism.addChild(establishFCDA);
                            establishFCDA.addChild(establishOCDM);

            propaganda.addChild(posters);
                posters.addChild(radioPresence);
                    radioPresence.addChild(tvPresence);
                posters.addChild(supportWestBerlin);

            spaceRace.addChild(launchExplorer1);
                launchExplorer1.addChild(mannedFlight);
                    mannedFlight.addChild(moonMission);
                        moonMission.addChild(buildISS);
                            buildISS.addChild(apolloSoyuz); //reqiuires radio pres sat imag sd1 III icbm
        } else {
            var warsawPact = new Node(player, "warsaw", "warsaw.jpg", "Warsaw pact", "Propose the Warsaw Pact to the other Communist nations. Any nation who accepts will benefit from the alliance, by getting a permanent 5% boost to their defence, which will reduce enemy hit chance.", false, 0, 1000, false, false, function() {
                var event = new Event("warsaw", "Join the Warsaw pact", player.name + " has proposed the Warsaw pact. This provides a defense boost. It costs 1000 money", "Sign me up", "Not a chance, bucko", 1, function(participants) {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id === player.id || p.money > 1000) {
                            if (p.id !== player.id) p.money -= 1000;
                            p.defBonus += 5;
                            p.addLog("You have joined the Warsaw pact!");
                        } else {
                            p.addLog("You didn't have enough money to join the Warsaw pact! Oh well. You're on your own kid.");
                        }
                    }
                    game.globalLog("The Warsaw pact has formed!");
                });

                event.addParticipant(player);

                game.eventManager.addEvent(event);
                game.lockTech("warsaw");
            });
            var attackSouthKorea = new Node(player, "korea", "sk.png", "Attack South Korea", "Send troops to crush South Korea. This will cost you population for the war, and if the capitalists decide to defend S.K., you may lose the war. If you win, you gain the population and money of S.K.", false, 0, 2000, false, false, function() {
                var event = new Event("sk", "Communists Invade South Korea", "The communists are invading South Korea. If you are communist, you may join your comrades. If you are capitalist, you can choose to defend South Korea. Winners gain the population and money of South Korea, as well as a boost to happiness. If you lose, the population and money you invested go bye-bye. Regardless of your allegiance, the more allies you have in the fight, the greater chance of success you have. This costs 30 population and 2000 money.", "Move out!", "Nah. Too riskay.", false, function(participants) {
                    var counts = [0, 0];
                    for (var i = 0; i < participants.length; i++) {
                        counts[participants[i].allegiance]++;
                    }
                    var victory = -1;
                    if (counts[0] <= 0) victory = 1;
                    if (counts[1] <= 0) victory = 0;
                    if (victory === -1) {
                        var normalizedDefense = counts[0] / (game.maxPlayers / 2);
                        var normalizedOffense = counts[1] / (game.maxPlayers / 2);
                        if (Math.random() < normalizedOffense) {
                            victory = 1;
                        } else {
                            if (Math.random() < normalizedDefense) {
                                victory = 0;
                            } else {
                                victory = Math.random() > 0.5 ? 0 : 1;
                            }
                        }
                    }

                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id !== player.id) {
                            if (p.money > 2000) {
                                p.money -= 2000;
                            } else {
                                p.addLog("You did not have enough money to help in South Korea");
                                continue;
                            }
                        }
                        if (p.allegiance === victory) {
                            p.population += 20;
                            p.happiness += 20;
                            p.addLog("Hooray! You won in South Korea! You got an additional 20 population and 20 happiness!");
                        } else {
                            p.population -= 30;
                            p.happiness -= 5;
                            p.addLog("Damn. You lost pretty bad in South Korea... You lose 30 population and 5 happiness...");
                        }
                    }

                    game.globalLog("The " + (victory === 0 ? "capitalists" : "communists") + " won in South Korea!");

                });

                event.addParticipant(player); //They can't weasle out of this one.

                game.eventManager.addEvent(event);
                game.lockTech("korea");
            });
            var putDownHungary = new Node(player, "hungary", "rev.jpg", "Put Down Hungarian Revolution", "Send troops to crush the Hungarian Revolution that is brewing. You loose 5 population every turn that this is not active. It does, however, lose you happiness.", false, 0, 500, false, false, function() {
                player.popAdd += 5;
                player.happiness -= 5;
            });
            var joinAfghanistan = new Node(player, "afghan", "afghanistan.png", "Join war in Afghanistan", "Try to put down the multinational insurgency. You loose 5 population every turn that this is not active.", false, 0, 2000, false, false, function() {
                player.popAdd += 5;
            });
            var a35 = new Node(player, "a35", "abm.jpg", "A-35 Anti Ballistic Missiles", "Unlocks the purchase of the A-35 ABM, which reduces the hit chance of capitalist attack by 5% each", false, 100, 500, ["a35"], this.createItemIntel(player, "a35"));
            var a35m = new Node(player, "a35m", "abm.jpg", "A-35m Anti Ballistic Missiles", "An upgraded version of the A-35 system of defence. This system reduces hit chance by 10% per purchased ABM.", false, 200, 850, ["a35m"], this.createItemIntel(player, "a35m"));
            var a135 = new Node(player, "a135", "abm.jpg", "A-135 Anti Ballistic Missiles", "The final stage of your missile defence system, and the most effective. This stage gives a base 10% hit reduction, due to the upgraded radar involved with it’s implementation, and you can purchase new missiles which each reduce hit chance by 30%", false, 450, 1200, ["a135"], this.createItemIntel(player, "a135"));

            var kgb = new Node(player, "kgb", "kgb.png", "KGB", "The KGB leads both Espionage and Counter Espionage. They provide spies, initially equal to the number of enemy players. They have a 50% chance to steal information when placed in an enemy state, and a 50% chance to prevent information from being stolen. The counter-espionage division also has a 1% chance to discover enemy spies every turn.", true);
            var stasiInformants = new Node(player, "stasi", "stasi.png", "Stasi informants", "Create a system of informants, who report to the Stasi, in essence a subsidiary of the KGB. This will cost your population happiness, but it will help discover enemy spies, increasing the rate at which you catch them by 20%.", false, 0, 300, false, false, function() {
                player.happiness -= 20;
                player.spyBoost += 20;
            });
            var civilDefenseTroops = new Node(player, "civil_defense", "deftroops.jpg", "Civil defence troops", "Establish the Civil Defense Troops, a reserve force called to action in case of disaster. They reduce the population loss of incoming attacks by 20%", false, 0, 1050, false, false, function() {
                player.popDefIncrease += 20;
            });
            var buildNuclearBunkers = new Node(player, "bunkers", "bunker.jpg", "Build nuclear bunkers", "Build a series of nuclear bunkers across your nation. This reduces the population loss of incoming attacks by 20%. It is stackable with Civil Defense Troops.", false, 0, 2500, false, false, function() {
                player.popDefIncrease += 20;
            });
            var increaseInternationalPropaganda = new Node(player, "int_prop", "interprop.png", "Increase international propaganda", "Focus more of your resources on influencing the \"outside.\" This will not affect your population’s happiness; it will be an added cost per turn, and will decrease the happiness of capitalist nations.", false, 0, 0, false, false, function() {
                player.moneyUpkeep += 100; //TODO I don't know how much this is. It's probably in the doc but I'm too lazy right now.
                for (var i = 0; i < game.players.length; i++) {
                    var p = game.players[i];
                    if (p.allegiance === 0) {
                        p.happiness -= 10;
                        p.addLog(player.name + " has increased their international propaganda. Your happiness has decreased.");
                    }
                }
            });

            var sputnik = new Node(player, "sputnik", "sputnik.jpg", "Launch Sputnik", "Launch the satellite Sputnik. This provides a happiness boost", false, 0, 0, false, false, function() {
                player.happiness += 10;
            });
            var buildMir = new Node(player, "mir", "mir.jpg", "Construct MIR", "Participate in building MIR", false, 600, 600, false, false, function() {
                var event = new Event("mir", "Construct MIR", player.name + " has proposed MIR. Would you like to add your funds and science to the project? Your science will increase more every turn (5%) due to collaboration with other nations, but it will have a one time cost of 500 science and 500 money.", "I would love to", "Nope", 1, function(participants) {
                    for (var i = 0; i < participants.length; i++) {
                        var p = participants[i];
                        if (p.id !== player.id) {
                            if (p.science > 500 && p.money > 500) {
                                p.moneyBoost += 5;
                                p.science -= 500;
                                p.money -= 500;
                            } else {
                                p.addLog("You did not have enough money and science to participate in the building of MIR");
                            }
                        }
                    }
                    player.moneyBoost += 5;
                    game.globalLog("The communists built MIR!");
                });

                event.addParticipant(player); //They can't weasle out of this one.

                game.eventManager.addEvent(event);
                game.lockTech("mir");
            });

            //--- Special Prereqs ---
            proposeSTART.prereqs = [a135, buildNuclearBunkers, advancedCommunication];
            attemptBlackmail.prereqs = [civilDefenseTroops, tvPresence, moonMission];
            apolloSoyuz.prereqs = [tvPresence, satImagery, civilDefenseTroops];

            //--- Special descs ---
            posters.desc = "You commission anti-capitalist posters and hang them around the city, increasing happiness.";
            radioPresence.desc = "Radios will now play anti-capitalist ads and discuss government-suggested topics. This further increases happiness, but also increases the negative effects of capitalist victory.";
            tvPresence.desc = "You start showing \"duck and cover\" films, as well as other television propaganda. This magnifies the effect of radio ads";
            apolloSoyuz.desc = "Allow the capitalists access to your space technology in return for your own. If all other countries agree to your proposal, the cold war ends. Otherwise, participating nations each gain a free technology as a result of new information.";

            aBomb.addChild(warsawPact);
            hBomb.addChild(attackSouthKorea);
            hBomb.addChild(putDownHungary);
            icbm.addChild(joinAfghanistan);
            mirv.addChild(proposeSTART);

            defenseBranch.addChild(a35);
                a35.addChild(a35m);
                    a35m.addChild(a135);

            espionageBranch.addChild(kgb);
                kgb.addChild(satImagery);
                    satImagery.addChild(expandAgency);
                        expandAgency.addChild(advancedCommunication);
                            advancedCommunication.addChild(attemptBlackmail);
                kgb.addChild(civilDefenseTroops);
                kgb.addChild(buildNuclearBunkers);
                kgb.addChild(stasiInformants);

            propaganda.addChild(posters);
                posters.addChild(radioPresence);
                    radioPresence.addChild(tvPresence);
                        tvPresence.addChild(increaseInternationalPropaganda);

            spaceRace.addChild(sputnik);
                sputnik.addChild(mannedFlight);
                    mannedFlight.addChild(moonMission);
                        moonMission.addChild(buildMir);
                            buildMir.addChild(apolloSoyuz);

        }
        return tree;
    },
    createItemIntel: function(player, i) {
        var item = player.itemRegistry.getItem(i);

        return [
            new Intel(item.name + "s", "WBR", false, false, function(intel, unlocker) {
                intel.desc = player.name + " has " + item.count + " " + item.name + "s";
            }),
            new Intel(item.name + "s Estimate", "WBR", false, false, function(intel, unlocker) {
                var count = item.count - Math.floor(Math.random() * 10);
                intel.desc = player.name + " has more than " + count + " " + item.name + "s. We don't know how many...";
            }),
            new Intel(item.name + "s Estimate", "WBR", false, false, function(intel, unlocker) {
                var count = item.count + Math.floor(Math.random() * 10);
                intel.desc = player.name + " has less than " + count + " " + item.name + "s. We don't know how many...";
            })
        ];
    }
})
