var EventSrc = require("../../event/eventsrc");

module.exports = Node = EventSrc.extend({
    init: function(owner, id, thumb, name, desc, unlocked, sciPrice, moneyPrice, itemUnlocks, intelUnlocks, onUnlock) {
        this.owner = owner;
        this.id = id;
        this.thumb = thumb;
        this.name = name;
        this.desc = desc;
        this.unlocked = unlocked;
        this.prereqs = false;
        this.sciPrice = sciPrice;
        this.moneyPrice = moneyPrice;
        this.subNodes = [];
        this.itemUnlocks = itemUnlocks;
        this.intelUnlocks = intelUnlocks;
        this.onUnlock = onUnlock;

        this.locked = false;
    },
    isUnlockable: function() {
        if (this.locked) return false;
        if (this.prereqs === false) return true;

        for (var i = 0; i < this.prereqs.length; i++) {
            if (this.prereqs[i].unlocked === false) return false;
        }
        return true;
    },
    unlock: function(){
        if (this.isUnlockable()) {
            this.unlocked = true;
            if (this.onUnlock !== undefined) {
                this.onUnlock(this);
            }
            if (this.itemUnlocks !== false) {
                for (var i = 0; i < this.itemUnlocks.length; i++) {
                    var item = this.owner.itemRegistry.getItem(this.itemUnlocks[i]);
                    if (item !== false) item.unlock();
                }
            }
            if (this.intelUnlocks !== false) {
                for (var i = 0; i < this.intelUnlocks.length; i++) {
                    this.owner.intelManager.addAvailable(this.intelUnlocks[i]);
                }
            }
        }
    },
    addChild: function(node){
        this.subNodes.push(node);
    },
    /*
     * (Seth) Traverses entire sub tree to find if ID is unlockable, if it exists.
     */
    isChildUnlockable: function(id) {
        if (id === this.id && this.isUnlockable()) return true;
        for (var i = 0; i < this.subNodes.length; i++) {
            if (this.subNodes[i].isChildUnlockable(id)) return true;
        }
        return false;
    },
    isChildUnlocked: function(id) {
        var child = this.getChild();
        if (child !== false) return child.unlocked;
        return false;
    },
    getChild: function(id) {
        if (id === this.id) return this;
        for (var i = 0; i < this.subNodes.length; i++) {
            var c = this.subNodes[i].getChild(id);
            if (c !== false) return c;
        }
        return false;
    },
    getAllUnlockable: function() {
        var available = [];
        for (var i = 0; i < this.subNodes.length; i++) {
            var n = this.subNodes[i];
            if (n.unlocked) {
                available = available.concat(n.getAllUnlockable());
            } else if (n.isUnlockable()) {
                available.push(n);
            }
        }
        return available;
    }
});
