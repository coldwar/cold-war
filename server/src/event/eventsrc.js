var Class = require("../lib/class").Class;

module.exports = EventSource = Class.extend({
    init: function() {
        this.callbacks = {};
    },
    on: function(event, callback) {
        if (this.callbacks[event] == null) this.callbacks[event] = [];
        this.callbacks[event].push(callback);
    },
    dispatch: function(event, data) {
        //console.log("Event dispatch '" + event + "'");
        if (this.callbacks[event] != null) {
            for (var i = 0; i < this.callbacks[event].length; i++) {
                this.callbacks[event][i](data);
            }
        }
    }
});
