define(["net/nethandler", "screens/lobby", "screens/gameplay"], function(NetHandler, Lobby, Gameplay) {

var Game = Class.extend({
    init: function() {
        this.net = new NetHandler();
        this.lobby = new Lobby(this);
        this.gameplay = new Gameplay(this);
        console.log("Created new game instance.");
    },

    run: function() {
        this.setScreen("scr-login", true);
        this.net.connect();

        var server = this.net.server;
        var self = this;
        $("#login-submit").click(function() {
            var username = $("#login-name").val();
            server.emit("login", {user: username});
            self.username = username;
        });

        server.on("login_success", function(data) {
            self.setScreen("scr-login", false);
            self.setScreen("scr-lobby", true);
            self.lobby.run();
            self.gameplay.run();
        });

        server.on("game_start", function(data) {
            self.setScreen("scr-lobby", false);
            self.setScreen("scr-game", true);
        });

        server.on("game_end", function(data) {
            if (data.victory !== undefined) {
                alert(data.victory);
            }
            
            self.setScreen("scr-lobby", true);
            self.setScreen("scr-game", false);
        })

        server.on("login_fail", function(data) {
            //Temp alert
            alert(data.reason);
        });
    },

    setScreen: function(screen, toggle) {
        var scr = $("#" + screen);
        if (toggle) {
            scr.addClass("visible");
            scr.css("height", "auto");
            var height = scr.height();
            scr.css("height", "0");
            scr.animate({
                height: height
            }, 600, function() {
                scr.css("height", "auto");
            });
        } else {
            scr.animate({
                height: 0
            }, 600, function() {
                scr.removeClass("visible");
            });
        }
    }
});

return Game;

});
