define(["lib/socketio"], function(io) {
    var NetHandler = Class.extend({
        init: function() {
            this.server = false;
        },
        connect: function(handler) {
            var host = "//" + window.location.hostname;
            console.log("Connecting to server " + host);
            this.server = io(host);
            if (handler !== undefined) {
                this.server.on("connect", function() {
                    handler(self.server);
                });
            }
        }
    });

    return NetHandler;
});
