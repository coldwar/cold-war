define(function() {

var Gameplay = Class.extend({
    init: function(game) {
        this.game = game;
    },
    run: function() {
        var server = this.game.net.server;
        var self = this;

        var tutorial = true;
        var tutorialCounter = 0;
        var tutorialCounterMax = 6;

        $(".spy-pl-def").click(function() {
            var op = $(this).data("op");
            var tfield = $("#spy-alloc-defense");
            self.moveSpies(tfield, op);
        });

        server.on("game_start", function(data) {
            self.cleanup();
            self.purchaseQueue = [];
            self.techQueue = [];
            self.eventQueue = [];

            $("#game-end-turn").addClass("disabled");

            if (data.allegiance === 0) {
                $("#game-end-turn").removeClass("btn-danger");
                $("#game-end-turn").addClass("btn-primary");
            } else {
                $("#game-end-turn").removeClass("btn-primary");
                $("#game-end-turn").addClass("btn-danger");
            }

            self.allegiance = data.allegiance;

            self.otherPlayers = data.others;

            if (tutorial) $("#modal-tutorial-intro").modal("show");
        });

        $("#tutorial-cancel").click(function() {
            tutorial = false;
        });

        $("#tutorial-continue").click(function() {
            $("#modal-tutorial-resources").modal("show");
        });

        $(".tab").click(function() {
            tut = $(this).data("tut");
            if (tutorial && tut !== undefined) {
                var m = $("#modal-tutorial-" + tut);
                if (m.data("done") === undefined) {
                    m.modal("show");
                    m.data("done", 1);
                }
            }
        });

        $(".tutorial-next").click(function() {
            tutorialCounter++;
            if (tutorialCounter === tutorialCounterMax) {
                $("#modal-tutorial-final").modal("show");
                tutorial = false;
            }
        });

        $(".pop-alloc button").click(function() {
            var op = $(this).data("op");
            var field = $(this).data("field");
            self.allocatePop(field, parseInt(op));
        })

        $("#game-end-turn").click(function() {
            $("#game-end-turn").addClass("disabled");

            var spyAllocs = {};

            $(".spy-alloc").each(function() {
                var id = $(this).data("id");
                var val = parseInt($(this).val());
                spyAllocs[id] = val;
            });

            var militaryAllocs = [];

            $(".military-player").each(function() {
                var player = $(this).data("player");
                var allocs = [];

                $(this).find("input").each(function() {
                    var weapon = $(this).data("weapon");
                    var count = $(this).val();
                    allocs.push({
                        weapon: weapon,
                        count: count
                    });
                });

                militaryAllocs.push({
                    player: player,
                    allocs: allocs
                });
            });

            server.emit("end_turn", {
                purchases: self.purchaseQueue,
                tech: self.techQueue,
                spyAllocs: spyAllocs,
                defSpies: parseInt($("#spy-alloc-defense").val()),
                events: self.eventQueue,
                militaryAllocs: militaryAllocs,
                moneyPopAlloc: parseInt($("#pop-alloc-money-field").val()),
                sciPopAlloc: parseInt($("#pop-alloc-science-field").val())
            });

            self.purchaseQueue = [];
            self.techQueue = [];
            self.eventQueue = [];
        });

        server.on("start_turn", function(data) {
            $("#game-end-turn").removeClass("disabled");

            self.money = data.money;
            self.science = data.science;
            self.happiness = data.happiness;
            self.population = data.population;
            self.spies = data.spies;
            self.updateResourceView();

            if (data.otherPlayers !== undefined) {
                $("#espionage-players").empty();
                for (var i = 0; i < data.otherPlayers.length; i++) {
                    var p = data.otherPlayers[i];
                    if (p.allegiance !== self.allegiance) {
                        var item = self.addEspionagePlayer(p.name, p.id);
                        $(item).click(function() {
                            var op = $(this).data("op");
                            var tfield = $(this).parents(".input-group").find(".spy-alloc");
                            self.moveSpies(tfield, op);
                        });
                    }
                }
            }

            $("#pop-available").html(data.popAvailable);
            $("#pop-alloc-money-field").val(data.moneyPopAlloc);
            $("#pop-alloc-science-field").val(data.sciPopAlloc);

            if (data.spyAllocs !== undefined) {
                for (player in data.spyAllocs) {
                    $('#spy-alloc-pl-' + player).val(data.spyAllocs[player]);
                }
            }

            $("#game-shop").empty();
            if (data.shops !== undefined) {
                for (var i = 0; i < data.shops.length; i++) {
                    var shop = data.shops[i];
                    var button = self.addStoreItem(shop.item, shop.cost, shop.thumb, shop.name, shop.desc, shop.count);
                    self.registerShop(shop, button);
                }
            } else {
                console.log("ERROR: Did not recieve any shop data from server!!!");
            }

            if (data.spyAllocs !== undefined) {
                for (id in data.spyAllocs) {
                    $("#spy-alloc-pl-" + id).val(data.spyAllocs[id]);
                }
            }

            for (var i = 0; i < data.logMessages.length; i++) {
                self.addJournalEntry(data.logMessages[i]);
            }

            $("#tech-tree-root").empty();
            for (var i = 0; i < data.techTree.length; i++) {
                var node = data.techTree[i];
                self.addTechNode(node.id, node.name, node.desc, node.thumb, node.unlocked, node.sciPrice, node.moneyPrice);
                self.exploreNode(node);
            }

            if (data.events !== undefined) {
                for (var i = 0; i < data.events.length; i++) {
                    self.registerEvent(data.events[i]);
                }
            }

            if (data.weapons !== undefined) {
                self.initMilitaryHeader(data.weapons);
                $("#military-players").empty();
                if (data.otherPlayers !== undefined) {
                    for (var i = 0; i < data.otherPlayers.length; i++) {
                        var p = data.otherPlayers[i];
                        if (p.allegiance !== self.allegiance) {
                            self.addMilitaryPlayer(p.id, p.name, data.weapons);
                        }
                    }
                }
            }
        });

    },
    allocatePop: function(field, amt) {
        var allocated = parseInt($("#pop-alloc-science-field").val()) + parseInt($("#pop-alloc-money-field").val());
        if (allocated + amt > this.population) return;
        var field = $("#pop-alloc-" + field + "-field");
        if (parseInt(field.val()) + amt < 0) return;
        field.val(parseInt(field.val()) + amt);
        $("#pop-available").html(this.population - (allocated + amt));
    },
    registerEvent: function(e) {
        var self = this;
        this.showEventModal(e.id, e.name, e.desc, e.confirm, e.deny, function() {
            self.eventQueue.push(e.id);
        });
    },
    exploreNode: function(node) {
        if (node.subNodes !== undefined) {
            for (var i = 0; i < node.subNodes.length; i++) {
                var n = node.subNodes[i];
                var button = this.addTechNode(n.id, n.name, n.desc, n.thumb, n.unlocked, n.sciPrice, n.moneyPrice, node.id);
                this.registerTech(n, button);

                this.exploreNode(n);
            }
        }
    },
    registerTech: function(node, button) {
        var self = this;
        $(button).click(function() {
            if (self.science >= node.sciPrice && self.money >= node.moneyPrice) {
                self.techQueue.push(node.id);
                self.science -= node.sciPrice;
                self.money -= node.moneyPrice;
                $(this).addClass("disabled");
                self.updateResourceView();
            }
        });
    },
    registerShop: function(shop, button) {
        var self = this;
        $(button).click(function() {
            if (shop.cost <= self.money) {
                self.purchaseQueue.push(shop.item);
                self.money -= shop.cost;
                var queue = $("#store-queue-" + shop.item);
                queue.html(parseInt(queue.html()) + 1);
                self.updateResourceView();
            }
        });
    },
    moveSpies: function(tfield, op) {
        //Pretty unsafe... We'll do some serverside checking for this.
        var val = parseInt(tfield.val());
        var mod = 0;
        if (op === "+" && this.spies > 0) mod = 1;
        if (op === "-" && val > 0) mod = -1;
        this.spies -= mod;
        val += mod;
        tfield.val(val);
        this.updateResourceView();
    },
    cleanup: function() {
        $("#game-shop").empty();
        $("#espionage-players").empty();
        $("#spy-alloc-defense").val("0");
        $("#log").empty();
        $("#tech-tree-root").empty();
        $("#military-players").empty();
        $("#military-header").empty();
    },
    updateResourceView: function() {
        $("#res-science").html(this.science);
        $("#res-money").html(this.money);
        $("#res-happiness").html(this.happiness);
        $("#res-population").html(this.population);
        $("#res-spies").html(this.spies);
        $("#spies-available").html(this.spies);
        $("#shop-money").html(this.money);
        $("#research-science").html(this.science);
        $("#research-money").html(this.money);
    },
    addStoreItem: function(item, cost, thumb, name, desc, count) {
        var html = "";
        html += '<div class="col-md-3">';
        html += '<div class="thumbnail store-item">';
        html += '<img class="store-thumb" src="assets/item-images/' + thumb + '" alt="' + name + '">';
        html += '<div class="caption">';
        html += '<h3>' + name + '</h3>';
        html += '<p>' + desc + '</p>';
        html += '<p>You have <span id="store-count-' + item + '">' + count + '</span></p>';
        html += '<p>Buying one costs ' + cost + '</p>';
        html += '<p>You will recieve <span id="store-queue-' + item + '">0</span> next turn</p>';
        html += '<p class="store-button-wrapper"><a href="javascript:void(0);" id="store-purchase-' + item + '" class="btn btn-' + (this.allegiance ? "danger" : "primary") + '" role="button">Purchase</a></p>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $("#game-shop").append(html);
        return "#store-purchase-" + item;
    },
    addEspionagePlayer: function(name, id) {
        var html = "";
        html += '<div class="input-group">';
        html += '<span class="input-group-addon">' + name + '</span>';
        html += '<input type="text" data-id="' + id + '" id="spy-alloc-pl-' + id + '" class="form-control spy-alloc" readonly placeholder="Allocated spies" value="0">';
        html += '<span class="input-group-btn">';
        html += '<button data-op="-" class="btn btn-default squarebtn esp-pl-' + id + '" type="button">-</button>';
        html += '<button data-op="+" class="btn btn-default squarebtn esp-pl-' + id + '" type="button">+</button>';
        html += '</span>';
        html += '</div>';
        html += "<p></p>"
        $("#espionage-players").append(html);
        return ".esp-pl-" + id;
    },
    initMilitaryHeader: function(weapons) {
        $("#military-header").empty();
        var html = '';
        html += "<th>Username</th>";

        for (var i = 0; i < weapons.length; i++) {
            html += '<th>' + weapons[i].name + '</th>';
        }
        $("#military-header").html(html);
    },
    addMilitaryPlayer: function(id, name, weapons) {
        var html = "";
        html += '<tr class="military-player" data-player="' + id + '" id="military-player-' + id + '">';
        html += '<td>'
        html += '<span class="lead mil-fixed">' + name + '</span>';
        html += '</td>';
        for (var i = 0; i < weapons.length; i++) {
            var w = weapons[i];
            html += '<td>';
            html += '<div class="input-group weapon-' + w.id + '">';
            html += '<span class="input-group-btn">';
            html += '<button data-op="-" class="btn squarebtn btn-default" type="button">-</button>';
            html += '</span>';
            html += '<input data-weapon="' + w.id + '" type="text" class="form-control" readonly placeholder="" value="0">';
            html += '<span class="input-group-btn">';
            html += '<button data-op="+" class="btn squarebtn btn-default" type="button">+</button>';
            html += '</span>';
            html += '</div>';
            html += '</td>';
        }
        html += '</tr>';
        $("#military-players").append(html);

        for (var i = 0; i < weapons.length; i++) {
            this.registerWeapon(id, weapons[i]);
        }
    },
    registerWeapon: function(id, weapon) {
        var w = "#military-player-" + id + " .weapon-" + weapon.id;

        var text = w + " input";
        //More dangerous shit that needs to be checked serverside :3
        $(w + " button").click(function() {
            var op = $(this).data("op");
            if (op === "+" && weapon.count > 0) {
                weapon.count--;
                $(text).val(parseInt($(text).val()) + 1);
            } else if (op === "-" && $(text).val() > 0) {
                weapon.count++;
                $(text).val(parseInt($(text).val()) - 1);
            }
        });

    },
    addJournalEntry: function(text) {
        var html = '<span class="log-entry">' + text + '</span>';
        var log = $("#log");
        log.append(html);
        log.scrollTop(log.prop('scrollHeight'));
    },
    addTechNode: function(id, name, desc, thumb, unlocked, sciPrice, moneyPrice, root) {
        var html = "";
        html += '<li class="media">';
        html += '<div class="media-left">';
        html += '<a href="javascript:void(0);">';
        html += '<img class="tech-thumb" class="media-object" src="assets/tech-images/' + thumb + '" alt="' + name + '">';
        html += '</a>';
        html += '</div>';
        html += '<div id="tech-' + id + '-node" class="media-body">';
        html += '<h4 class="media-heading">' + name + '</h4>';
        html += '<p>';
        html += desc;
        if (!unlocked) {
            html += '<p>';
            html += 'Science cost: ' + sciPrice + ", Money cost: " + moneyPrice;
            html += '<p>';
            html += '<button id="tech-buy-' + id + '" class="btn btn-' + (this.allegiance ? "danger" : "primary") + '" type="button">Purchase</button>'
        }
        html += '</div>';
        html += '</li>';

        var node = root === undefined ? $("#tech-tree-root") : $("#tech-" + root + "-node");
        node.append(html);
        return "#tech-buy-" + id;
    },
    showEventModal: function(id, title, desc, confirm, deny, onConfirm) {
        var html = "";
        html += '<div id="modal-event-' + id + '" class="modal fade" data-backdrop="static">';
        html += '<div class="modal-dialog">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<h4 class="modal-title">' + title + '</h4>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<p>Incoming event!</p>';
        html += '<p>';
        html += desc;
        html += '</p>';
        html += '<p>Good Luck.</p>';
        html += '</div>';
        html += '<div class="modal-footer">';
        html += '<button type="button" class="btn btn-danger deny" data-dismiss="modal">' + deny + '</button>';
        html += '<button type="button" class="btn btn-success confirm" data-dismiss="modal">' + confirm + '</button>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $("#event-modal-container").append(html);

        var modal = "#modal-event-" + id;

        $(modal).on("hidden.bs.modal", function() {
            $(this).remove();
        });

        $(modal + " .confirm").click(onConfirm);

        $(modal).modal("show");

        return modal;
    }
});

return Gameplay;

});
