define(function() {

var Lobby = Class.extend({
    init: function(game) {
        this.game = game;
        this.selectedID = 0;
        this.joinedID = false;
    },
    run: function() {
        var self = this;

        var server = this.game.net.server;

        $("#newgame-submit").click(function() {
            var name = $("#newgame-name").val();
            if (name === "") return;
            var maxPlayers = $("#newgame-max-players").val();
            server.emit("new_game", {
                name: name,
                maxPlayers: maxPlayers
            });
            $("#modal-newgame").modal("hide");
        });

        $("#lobby-connect").click(function() {
            server.emit("join_game", {
                id: self.selectedID
            });
        });

        server.on("game_list", function(data) {
            $("#lobby-gamelist").empty();
            for (var i = 0; i < data.length; i++) {
                var game = data[i];
                self.createGameListElement(game.id, game.name, game.playerCount, game.maxPlayers);
            }
            $(".lobby-gamelist-element").click(function() {
                var id = $(this).data("id");
                self.setSelected(id);
            });
            self.setSelected(self.selectedID);
        });

        server.on("game_joined", function(data) {
            self.joinedID = data.id;
            self.setSelected(data.id);
        });

        server.on("game_info", function(data) {
            if (data.id === self.selectedID) {
                $("#lobby-game-players").empty();
                for (var i = 0; i < data.players.length; i++) {
                    var p = data.players[i];
                    self.createGamePlayerElement(p.name, p.allegiance);
                }
            }
        });

        server.on("allegiance", function(a) {
            $("#lobby-allegiance").html(a === 0 ? "Capitalist" : "Communist");
        });

        server.on("player_list", function(data) {
            $("#lobby-player-list").empty();
            for (var i = 0; i < data.length; i++) {
                self.createPlayerListElement(data[i]);
            }
        });
    },
    setSelected: function(id) {
        this.selectedID = id;
        $(".lobby-gamelist-element").removeClass("active");
        $("#lobby-gamelist-e" + id).addClass("active");
        if (id === this.joinedID) {
            $("#lobby-connect").addClass("hidden");
        } else {
            $("#lobby-connect").removeClass("hidden");
        }
        this.game.net.server.emit("get_game_info", {id: id});
    },
    createGameListElement: function(id, name, players, maxPlayers) {
        var html = "";
        html += '<a href="javascript:void(0);" class="list-group-item lobby-gamelist-element" id="lobby-gamelist-e' + id + '" data-id="' + id + '">';
        html += name;
        html += '<span class="badge">' + players + '/' + maxPlayers + '</span>';
        html += '</a>';
        $("#lobby-gamelist").append(html);
    },
    createGamePlayerElement: function(name, allegiance) {
        var html = "";
        html += '<tr>';
        html += '<td>' + name + '</td>';
        var al = allegiance === 0 ? "Capitalist" : "Communist";
        html += '<td>' + al + '</td>';
        html += '</tr>';
        $("#lobby-game-players").append(html);
    },
    createPlayerListElement: function(name) {
        var html = '<li class="list-group-item">' + name + '</li>';
        $("#lobby-player-list").append(html);
    }
});

return Lobby;

});
